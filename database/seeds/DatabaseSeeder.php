<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $clientsNames = ['Acme', 'Apple', 'Microsoft'];
        $productsNames = ['The Matrix Tr', 'Macbook Air', 'Server Rack', 'Server Farm', 'Watch'];

        DB::table('clients')->insert(
            collect($clientsNames)->map(function ($name) {
                return compact('name');
            })->toArray()
        );

        DB::table('products')->insert(
            collect($productsNames)->map(function ($name) {
                return compact('name');
            })->toArray()
        );

        $clients = DB::table('clients')
            ->whereIn('name', $clientsNames)
            ->orderByDesc('id')
            ->limit(count($clientsNames))
            ->get()
            ->pluck('id', 'name');

        $products = DB::table('products')
            ->whereIn('name', $productsNames)
            ->orderByDesc('id')
            ->limit(count($productsNames))
            ->get()
            ->pluck('id', 'name');

        DB::table('orders')->insert(
            array_merge(
                collect(range(0, 18))->map(function ($i) use ($products, $clients) {
                    return [
                        'client_id' => $clients['Acme'],
                        'product_id' => $products['The Matrix Tr'],
                        'total' => $i + 30,
                        'date' => Carbon::parse('2/1/2015')->addDays($i)->toDateString(),
                    ];
                })->toArray(),
                collect(range(0, 18))->map(function ($i) use ($products, $clients) {
                    return [
                        'client_id' => $clients['Microsoft'],
                        'product_id' => $products['Macbook Air'],
                        'total' => $i + 1200,
                        'date' => Carbon::parse('2/19/2015')->addDays($i)->toDateString(),
                    ];
                })->toArray(),
                [
                    [
                        'client_id' => $clients['Apple'],
                        'product_id' => $products['Server Rack'],
                        'total' => 10000,
                        'date' => Carbon::parse('2/10/2015')->toDateString()
                    ],
                    [
                        'client_id' => $clients['Apple'],
                        'product_id' => $products['Server Farm'],
                        'total' => 100000,
                        'date' => Carbon::parse('2/28/2015')->toDateString()
                    ],
                    [
                        'client_id' => $clients['Apple'],
                        'product_id' => $products['Watch'],
                        'total' => 399,
                        'date' => Carbon::parse('3/9/2015')->toDateString()
                    ]
                ]
            )
        );
    }
}
