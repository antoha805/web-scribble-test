<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'client_id' => 'bail|required|integer|min:1|exists:clients,id',
            'product_id' => 'bail|required|integer|min:1|exists:products,id',
            'total' => 'required|integer|min:1',
            'date' => 'required|date'
        ];
    }
}
