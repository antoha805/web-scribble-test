<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'q' => array_merge(
                ['nullable', 'required_with:search_by'],
                [in_array($this->input('search_by'), ['client_name', 'product_name']) ? ['string'] : []],
                [$this->input('search_by') === 'total' ? ['integer', 'min:1'] : []],
                [$this->input('search_by') === 'date' ? ['date'] : []]
            ),
            'search_by' => [
                [
                    'nullable',
                    'required_with:q',
                    Rule::in([
                        'all',
                        'client_name',
                        'product_name',
                        'total',
                        'date'
                    ])
                ]
            ],
            'order_by' => [
                    'nullable',
                    Rule::in([
                        'client_name_asc',
                        'client_name_desc',
                        'product_name_asc',
                        'product_name_desc',
                        'total_asc',
                        'total_desc',
                        'date_asc',
                        'date_desc'
                    ])
            ],
        ];
    }
}
