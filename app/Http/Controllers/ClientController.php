<?php

namespace App\Http\Controllers;

use App\Http\Resources\Client as ClientResource;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $clientRepository = null;

    /**
     * Create a new controller instance.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return ClientResource::collection($this->clientRepository->getList());
    }
}
