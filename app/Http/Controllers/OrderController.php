<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderList;
use App\Http\Requests\UpdateOrder;
use App\Http\Resources\Order as OrderResource;
use App\Notifications\OrdersReport;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    private $orderRepository = null;

    /**
     * Create a new controller instance.
     *
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param OrderList $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(OrderList $request)
    {
        $paginatedOrders = $this->orderRepository->getList($request->all());

        return response()->json([
            'data' => [
                'orders' => OrderResource::collection($paginatedOrders),
                'chart_data' =>$this->orderRepository->getChartData($request->all())
            ],
            'meta' => [
                'total' => $paginatedOrders->total(),
                'per_page' => $paginatedOrders->perPage(),
                'last_page' => $paginatedOrders->lastPage()
            ]
        ]);
    }



    /**
     * @param UpdateOrder $request
     * @param int $id
     * @return OrderResource
     */
    public function update(UpdateOrder $request, int $id)
    {
        return new OrderResource($this->orderRepository->update($id, $request->all()));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return OrderResource
     */
    public function destroy(Request $request, int $id)
    {
        return new OrderResource($this->orderRepository->destroy($id));
    }

    /**
     * @param OrderList $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function report(OrderList $request)
    {
        Notification::route('mail', 'taylor@example.com')
            ->notify(new OrdersReport($this->orderRepository->getList($request->all())));

        return response(null, 200);
    }
}
