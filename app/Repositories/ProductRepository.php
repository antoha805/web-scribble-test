<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    /**
     * @return object
     */
    public function getList(): object
    {
        return Product::all();
    }
}
