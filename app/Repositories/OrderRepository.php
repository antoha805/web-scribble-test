<?php

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    /**
     * @param array $params
     * @return object
     */
    public function getList(array $params = []): object
    {
        $query = Order::select('orders.*')
            ->withRelations()
            ->filter(Arr::only($params, ['q', 'search_by']));

        if (isset($params['order_by'])) {

            $query = $query->orderBy(
                preg_replace("/_(asc|desc)$/i", '', $params['order_by']),
                (substr($params['order_by'], -strlen('asc')) === 'asc' ? 'asc' : 'desc')
            );
        }

        return $query->paginate();
    }

    /**
     * @param array $params
     * @return object
     */
    public function getChartData(array $params = []): object
    {
        return Order::select('orders.date', DB::raw('sum(orders.total) as total_sum'))
            ->filter(Arr::only($params, ['q', 'search_by']))
            ->groupBy('orders.date')
            ->orderBy('orders.date')
            ->get();
    }

    /**
     * @param int $id
     * @param array $data
     * @return Order
     */
    public function update(int $id, array $data): Order
    {
        Order::findOrFail($id)->update($data);

        return Order::select('orders.*')->withRelations()->where(['orders.id' => $id])->first();
    }

    /**
     * @param int $id
     * @return Order
     */
    public function destroy(int $id): Order
    {
        $order = Order::findOrFail($id);

        return $order->delete()
            ? $order
            : null;
    }
}
