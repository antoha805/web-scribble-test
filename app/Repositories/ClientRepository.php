<?php

namespace App\Repositories;

use App\Models\Client;

class ClientRepository
{
    /**
     * @return object
     */
    public function getList(): object
    {
        return Client::all();
    }
}
