<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'product_id', 'total', 'date'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){

        return $this->belongsTo(Client::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(){

        return $this->belongsTo(Product::class);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->toDateString();
    }

    /**
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeWithRelations(Builder $query)
    {
        return $query
            ->addSelect('clients.name as client_name')
            ->addSelect('products.name as product_name')
            ->join('clients', 'clients.id', '=', 'orders.client_id')
            ->join('products', 'products.id', '=', 'orders.product_id');
    }

    /**
     * @param Builder $query
     * @param array $params
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $params)
    {
        if (!isset($params['q']) || !isset($params['search_by'])) {
            return $query;
        }
        if (in_array($params['search_by'], ['client_name', 'all'])) {
            $query = $query->orWhereHas('client', function (Builder $query) use ($params) {
                $query->where('name', '=', $params['q']);
            });
        }
        if (in_array($params['search_by'], ['product_name', 'all'])) {
            $query = $query->orWhereHas('product', function (Builder $query) use ($params) {
                $query->where('name', '=', $params['q']);
            });
        }
        if (in_array($params['search_by'], ['total', 'all']) && ((int) $params['q']) == $params['q']) {
            $query = $query->orWhere('total', $params['q']);
        }
        if (in_array($params['search_by'], ['date', 'all'])) {
            try {
                $query = $query->orWhere('date', Carbon::parse($params['q'])->toDateString());
            } catch (\Exception $e) {
            }
        }

        return $query;
    }
}
