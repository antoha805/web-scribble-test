import Orders from '../components/Orders.vue';

export const routes = [
    {
        path: '/',
        name: 'transactions',
        component: Orders,
        meta: {requiresAuth: true}
    },
];
