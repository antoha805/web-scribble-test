import {routes} from './routes'
import VueRouter from 'vue-router'

export const router = new VueRouter({
    routes: routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});
