// getters
const getters = {

}

const state = {
    list: null
}

// actions
const actions = {

    async getList({ state, commit, rootState }, params) {

        commit('setList', null);

        return new Promise((resolve, reject) => {

            axios.get('/api/products', {params}).then((response) => {

                commit('setList', response.data.data);

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
};

// mutations
const mutations = {

    setList (state, list) {
        state.list = list;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
