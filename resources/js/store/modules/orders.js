// getters
const getters = {

}

const state = {
    list: null,
    chartData: null,
    meta: null
}

// actions
const actions = {

    async getIndexPageData({ state, commit, rootState }, params) {

        commit('setList', null);
        commit('setChartData', null);
        commit('setMeta', null);

        return new Promise((resolve, reject) => {

            axios.get('/api/orders', {params}).then((response) => {

                commit('setList', response.data.data.orders);
                commit('setChartData', response.data.data.chart_data);
                commit('setMeta', response.data.meta);

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
    async destroy({ state, commit, rootState }, order) {

        return new Promise((resolve, reject) => {

            axios.delete(`/api/orders/${order.id}`).then((response) => {

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
    async update({ state, commit, rootState }, params) {

        return new Promise((resolve, reject) => {

            axios.put(`/api/orders/${params.id}`, _.omit(params, ['id'])).then((response) => {

                if (state.list){
                    commit('setList', _.map(state.list, ord => +ord.id === +response.data.data.id ? response.data.data : ord))
                }

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
    async report({ state, commit, rootState }, params) {

        return new Promise((resolve, reject) => {

            axios.post(`/api/orders/report`, params).then((response) => {

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
};

// mutations
const mutations = {

    setList (state, list) {
        state.list = list;
    },
    setChartData (state, data) {
        state.chartData = data;
    },

    setMeta (state, meta) {
        state.meta = meta;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
