import Vue from 'vue'
import Vuex from 'vuex'

import orders from './modules/orders'
import clients from './modules/clients'
import products from './modules/products'

Vue.use(Vuex);

export const store = new Vuex.Store({
        modules: {
            orders,
            clients,
            products
        },
        actions: {

        },
        mutations: {

        },
    });
