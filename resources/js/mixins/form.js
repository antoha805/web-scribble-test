export const form = {
    data() {
        return {
            isSubmitting: false,
            serverValidationErrors: {},
            responseMessage: null,
            responseStatus: null,
            formFields: {},
        }
    },
    created() {
    },
    methods: {
        beforeSubmitHandle() {
            this.serverValidationErrors = {};
            this.isSubmitting = true;
            this.responseMessage = null;
            this.responseStatus = null;
        },
        submitErrorHandle(error) {
            this.isSubmitting = false;

            this.responseStatus = 'error';

            this.serverValidationErrors = _.get(error, 'response.data.errors');

            this.responseMessage = _.get(error, 'response.data.message', error.message);

        },
        submitSuccessHandle(response, ref = null){

            this.responseStatus = 'success';

            this.responseMessage = _.get(response, 'response.data.message');

            if (ref) {
                ref.reset();
            }
        },

        getValidationErrors(clientErrors, name){
              return _.values(_.get(this.serverValidationErrors, name)).concat(clientErrors || []);
        },
    },
    components: {
        'validation-errors': {
            template: `
                <div v-if="errors.length" class="invalid-feedback text-capitalize">
                    {{ errors[0] }}
                </div>`,
            props: ['errors'],
            name: 'validation-errors'
        },
    },
};
