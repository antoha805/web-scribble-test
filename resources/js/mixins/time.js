export const time = {

    data () {
        return {
            dateFormat: 'M/D/YYYY',
            dayFormat: 'hh:mm a'
        }
    },

    methods: {
        convertTime(value) {
            return {
                date: this.$moment(value).utc().format(this.dateFormat),
                time: this.$moment(value).utc().format(this.dateFormat)
            };
        },
    },
};
