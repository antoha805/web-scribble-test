/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuex from 'vuex';
Vue.use(Vuex);

Vue.use(require('vue-moment'));

import { ValidationProvider, extend, ValidationObserver } from "vee-validate";
import { required, min_value, integer } from "vee-validate/dist/rules";
extend('required', required);
extend('min_value', min_value);
extend('integer', integer);

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

import VueRouter from 'vue-router'
Vue.use(VueRouter);

import {router} from './route/router';
import {store} from './store';

import Toastr from 'vue-toastr';
require('vue-toastr/src/vue-toastr.scss');
Vue.component('vue-toastr', Toastr);

import VueCharts from 'vue-chartjs'
import { Line } from 'vue-chartjs'

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

//const files = require.context('./', true, /\.vue$/i)
//files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('pagination', require('./components/partials/Pagination.vue').default);
Vue.component('modal', require('./components/partials/Modal.vue').default);
Vue.component('order-form', require('./components/partials/forms/Order.vue').default);
// Vue.component('line-chart', require('./components/partials/LineChart.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import {time} from './mixins/time';

const app = new Vue({
    el: '#app',
    mixins: [time],
    store,
    template: `<div><router-view :key="+$store.state.key + '_' + $route.fullPath.replace(/\\?tab=\\d/, '')"></router-view><vue-toastr ref="toastr"></vue-toastr></div>`,
    router,
    data() {
        return {
            key: 0
        }
    },
    created () {
        extend('date', {
            validate: value => this.$moment(value, this.dateFormat, true).isValid(),
            message: `Date must be in ${this.dateFormat} format`
        });
    },
});
