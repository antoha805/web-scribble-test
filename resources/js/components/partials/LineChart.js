import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
    extends: Line,
    mixins: [reactiveProp],
    mounted () {
        this.renderChart(
            this.chartData,
            {
                responsive: true,
                type: 'line',
                maintainAspectRatio: false,
                aspectRatio: 2,
                legend: {
                    position: 'bottom',
                    labels: {
                        fontColor: '#fff',
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: "#293450",
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "#293450",
                        }
                    }]
                }
            }
        )
    }
}
