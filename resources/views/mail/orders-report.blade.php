@component('mail::message')
    <table>
        <tbody>
        <tr>
            <th>Client</th>
            <th>Product</th>
            <th>Total</th>
            <th>Data</th>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->client_name }}</td>
                <td>{{ $order->product_name }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ \Carbon\Carbon::parse($order->date)->format('m/d/Y') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endcomponent
